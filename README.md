# SISTEMA DE SEGURIDAD RESIDENCIAL ASEQUIBLE BASADO EN MICROCONTROLADOR CON ALERTA VÍA CORREO ELECTRÓNICO
#                                 Instituto Tecnológico de Veracruz

Integrantes:
- Cruz Tirado Sarahí.
- Ortega Michel Bismarck Jésus.
- Yamil Gabriel Díaz Rivera.
- Castro González José Jovani.

Como estudiantes del Instituto Tecnológico de Veracruz y proyecto final del curso de programación básica, buscamos crear un sistema de seguridad residencial asequible para poder reducir el porcentaje de robos en el estado de Veracruz.

Nuestro proyecto está realizado para funcionar con el MICROCONTROLADOR ESP32S, sensor PIR HC-SR501 y un buzzer que servirá como alarma.

La funcionalidad en general de nuestro proyecto será enviar un correo electrónico a usted, además, de forma opcional poder elegir si enviar un aviso a sus familiares o personas de confienza los cuales deben ser registrados; advertiendo sobre un posible intruso en su propiedad, evitando de esta manera poder sufrir perdidas de sus bienes o en el peor de los casos sufrir daño físico y emocional. Esto podrá llevarse a cabo gracias a nuestro sensor PIR HC-SR501, este módulo contiene un sensor Piroeléctrico, el cual puede detectar cambios de radiación infrarroja, si llega a detectar alguna anomalia el MICROCONTROLADOR ESP32S se encargará de realizar el envio del correo y sonar una alarma ininterrumpidamente y solo será posible desactivarla al reiniciar el sistema. Para el envío del correo se modificará una variable en el código para evitar que el sistema envíe una cantidad excensiva de correos y se llegue a considerar como SPAM.

EL objetivo del proyecto no solo es preservar su integridad, también buscamos hacerlo de la manera más asequible posible para que pueda implementarlo sin problema alguno en su residencia y recomendarlo a otras personas para preservar su seguridad e integridad. El presuspuesto total de la implementación de nuestro sistema de alarmas es de $308, este sistema podrá modificarlo y mejorarlo para que sea adaptado a sus necesidades.

En esta versión solo estará implementado lo anterior descrito (sensores PIR HC-SR501, buzzer), pero esperamos poder agregar mas funcionalidades como sensores humo, sismicos y cámaras a fin de poder monitorear desde su equipo de cómputo o dispositivo celular, teniendo de esta manera un sistema de seguridad más amplio, que cubra más de sus necesidades y gozar de un SISTEMA DE SEGURIDAD RESIDENCIAL ASEQUIBLE y completo.
