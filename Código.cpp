/*
Instituto Tecnológico de Veracruz
SISTEMA DE SEGURIDAD RESIDENCIAL ASEQUIBLE BASADO EN MICROCONTROLADOR CON ALERTA VÍA CORREO ELECTRÓNICO
Proyecto Final
Programación Básica EJ21 2F4C
Participantes:
    Sarahí Cruz Tirado
    Ortega Michel Bismarck Jesús
    Yamil Gabriel Díaz Rivera
    José Jovani Castro Gonzáles
Veracruz, Veracruz, Julio del 2021
*/
#include "ESP32_MailClient.h" //Incluimos librería para el cliente de correo

const char* Red = "Nombre de la red"; //Nombre de la Red WiFi a usar
const char* Pass = "Contraseña de la red"; //Contraseña de la red WiFi a usar

#define Servidor              "smtp.gmail.com" //Servidor del correo (Gmail en este caso)
#define Puerto                465 //Puerto SLL del Servidor SMPT para Gmail
#define Cuenta                "Remitente@gmail.com"    //Cuenta del dueño del correo
#define Contra                "Contraseña"            //Contraseña de la cuenta del dueño
#define Asunto                "Alerta de Seguridad"   //Asunto del correo
#define Destino               "Destinatario@gmail.com"//Persona a enviar mensaje en caso de irrupciones
                                                   
int Sensor = 32; //Pin del Sensor
int valor = LOW; //Variable de valor para el sensor iniciada en bajo
int bandera1 = 5; //Bandera1 
int bandera2 = 2; //Bandera 2
int buzzer_pin = 15; //Pin del Buzzer
int channel = 0;      //Canal del PWM (Pulse-With Modulation) Modulación por ancho de pulsos
int frequence = 2000;  //Frecuencia inicial
int resolution = 10;  //Resolución de 10 bits para ciclo de trabajo
unsigned long timer = millis(); //Tiempo en milisegundos desde el inicio del ESP32-S

TaskHandle_t sonar;  //Elemento de trabajo paralelo
eTaskState statusOf; //Estado de la tarea

SMTPData smtpData; //Objeto de datos del envío del correo
 
void Status(SendStatus info); //Función para estado

void Alarma(void *pvParameters){ //Programa para la Alarma y parámetros
    float sinVal;
    int   toneVal;
    for (byte t = 0; t<10;t++){ //Ciclo for con aumento de 10 por iteración
        for (byte x=0;x<180;x++){ 
            sinVal = (sin(x*(3.1412/180))); //Conversión de grados a radianes
            toneVal = 2000+(int(sinVal*1000)); //Generar la frecuencia
            ledcWriteTone(channel,toneVal);     //Configuración de la frecuencia
            delay(3); //Cada 2 ms se genera un nuevo tono
        }
    }
    ledcWriteTone(channel, 0);
    vTaskDelete(NULL); //Eliminar tarea
}

void setup(){
  pinMode(Sensor, INPUT); //Declaramos al Sensor como entrada
  Serial.begin(115200); //Configuración de baudios (Símbolos por segundo)
  Serial.println();
  Serial.print("Iniciando sistema"); //Mensaje de inicio
  WiFi.begin(Red, Pass); //Entramos a la red WiFi con el nombre y contraseña previamente definidos
  while (WiFi.status() != WL_CONNECTED) { //Mientras el estado sea diferente de conectado, esperar
    Serial.print("."); //Mensaje para Monitor Serie
    delay(5000); 
    }
  Serial.println();
  Serial.println("Conectado a la red"); //Mensaje al conectar
  Serial.println();
  Serial.println("Cargando envío"); //Segundo mensaje de confirmación
  Serial.println();
  ledcSetup(channel, frequence, resolution); //Función con canal, frecuencia y resolución
  ledcAttachPin(buzzer_pin, channel); //Canal PWM en GPIO del ESP32-S
  timer = millis();
  xTaskCreatePinnedToCore(Alarma, "Alarma", 5000, NULL, 1, &sonar, 0);//Tarea de alarmaq
}
void loop() {  //Función bucle
  valor = digitalRead(Sensor); //Llenar variable "valor" con la información del Sensor
  delay(1500); //Demora entre escaneos
  Serial.println(valor); ////Comprobar valor del Sensor
  if (valor == HIGH && bandera1 == 5){ //En caso de detectar algo y que nuestra bandera1 esté intacta
    Serial.println(bandera1); //Mensaje para corroborar valor de bandera1
    bandera1 = 4;   //Modificamos nuestra bandera1 con el valor de 4
    Serial.println(bandera1); //Corroboramos que se haya realizado la modificación
    EnviarMail(); //Llamamos función de EnviarMail
    bandera2 = 3; //Modificamos nuestra bandera2 con el valor de 3
  }
  if (valor == HIGH){ //Si "valor" detectó algo
    Serial.println("\n\t Movimiento detectado"); //Comentario para 
    valor = LOW; //Regresa "valor" a bajo
  }
  while(bandera2 == 3){ //Mientras bandera2 tiene el valor de 3
    if (millis()-timer >5000){
      statusOf = eTaskGetState(sonar); //Revisa el estado de la función de alarma
      if (statusOf == eReady){ //Si está lista
          timer = millis();
          xTaskCreatePinnedToCore(Alarma,"Alarma", 5000, NULL, 1, &sonar,0); //Llama la función de alarma
      }
    }
  }
}
void EnviarMail() { //Función del correo
  smtpData.setLogin(Servidor, Puerto, Cuenta, Contra); //Inicio de sesión en el correo con los datos declarados al inicio
  smtpData.setSender("Alarma Residencial", Cuenta); //Nombre del sistema
  smtpData.setPriority("High");  // Importancia o prioridad del correo
  smtpData.setSubject(Asunto); // Asunto del correo
    smtpData.setMessage("<div style=\"color:#dc4132;\"><center><h1>ALARMA ACTIVADA</h1></center></div><a><center><img src=\"https://proconsumidor.gob.do/wp-content/uploads/2018/04/alerta-de-seguridad-banner-1-2-720x340.jpg;\"></center></a><div style=\"color:#27a327;\"><p><center><h2>Se ha detectado un intruso. Llame a las autoridades competentes</h2></center></p></div>", true); //Mensaje del la alerta en formato HTML
  smtpData.addRecipient(Destino); //Destinatario previamente declarado
  //smtpData.addRecipient("DestinatarioAdicional@gmail.com"); // Destinatarios adicionales en caso de querer (opcional)
  smtpData.setSendCallback(Status); 
  if (!MailClient.sendMail(smtpData)) //En caso de tener errores
    Serial.println("Error debido a: " + MailClient.smtpErrorReason()); //Razón del error de envío de correo
    smtpData.empty();
}
void Status(SendStatus msg) { //Función para confirmar envío
  Serial.println(msg.info());
  if (msg.success()) { //Si el envío es confirmado
    Serial.println("Mensaje enviado correctamente"); //Señal de que funcionó
  }
}
